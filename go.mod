module codeberg.org/apapsch/go-slurp-files

go 1.18

require (
	codeberg.org/apapsch/go-aggregate-error v0.1.1
	github.com/pkg/sftp v1.13.5
	github.com/stretchr/testify v1.8.0
	golang.org/x/crypto v0.0.0-20220622213112-05595931fe9d
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/kr/fs v0.1.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/sys v0.0.0-20211216021012-1d35b9e2eb4e // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
